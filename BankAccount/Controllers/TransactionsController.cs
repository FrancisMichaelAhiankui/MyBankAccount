﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using BankAccount.Logic;
using BankAccount.Models;

namespace BankAccount.Controllers
{
    public class TransactionsController : ApiController
    {
        private AccountContext db = new AccountContext();

        // GET: api/Transactions
        public IQueryable<GetTransactionDTO> GetTransactions()
        {

            var transactions = from t in db.Transactions
                               select new GetTransactionDTO()
                               {
                                   AccountOwner = t.BankAccountName,
                                   Amount = t.Amount,
                                   Id = t.Id,
                                   TransactionDate = t.TransactionDate,
                                   TransactionType = t.TransactionType,
                               };
            
            return transactions;
        }

        // GET: api/Transactions/5
        [ResponseType(typeof(Transaction))]
        public async Task<IHttpActionResult> GetTransaction(long id)
        {
            Transaction transaction = await db.Transactions.FindAsync(id);
            if (transaction == null)
            {
                return NotFound();
            }

            return Ok(transaction);
        }

        // PUT: api/Transactions/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTransaction(long id, Transaction transaction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != transaction.Id)
            {
                return BadRequest();
            }

            db.Entry(transaction).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TransactionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Transactions
        [ResponseType(typeof(Transaction))]
        public async Task<IHttpActionResult> PostTransaction(Transaction transaction)
        {
            var transactions = db.Transactions.Where(a=>a.BankAccountName.ToLower() == transaction.BankAccountName.ToLower());
            var account = db.Accounts.FirstOrDefault(a => a.Name.ToLower() == transaction.BankAccountName.ToLower());
            DateTime dateTime = DateTime.Today;
            transaction.TransactionDate = dateTime;
            var transDay = transaction.TransactionDate.ToString("d");
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (transaction.TransactionType.ToLower() == "withdrawal")
            {
                var withdraw = db.Transactions.Where(a => a.TransactionType.ToLower() == "withdrawal");
                var maxWithdraw = withdraw.Where(a => a.TransactionDate.Day == transaction.TransactionDate.Day && 
                                        a.TransactionDate.Month == transaction.TransactionDate.Month &&
                                        a.TransactionDate.Year == transaction.TransactionDate.Year);
                var maxWithdrawPerDay = maxWithdraw.Sum(a => a.Amount);
                var maxFrequency = maxWithdraw.Count();
                if (maxWithdrawPerDay > 50000)
                {
                    return BadRequest("Max Withdrawal Per Day Limit Reached");
                }
                if (transaction.Amount > 20000)
                {
                    return BadRequest("Max Withdrawal Per Transaction Reached");
                }
                if (maxFrequency > 3)
                {
                    return BadRequest("Max Withdrawal Frequency Per Day Reached");
                }
                if (transaction.Amount > account.Balance)
                {
                    return BadRequest("Your balance is insufficient to make this transaction");

                }
                
            }
            if (transaction.TransactionType.ToLower() == "deposit")
            {
                var withdraw = db.Transactions.Where(a => a.TransactionType.ToLower() == "deposit");
                var maxWithdraw = withdraw.Where(a => a.TransactionDate.Day == transaction.TransactionDate.Day &&
                                        a.TransactionDate.Month == transaction.TransactionDate.Month &&
                                        a.TransactionDate.Year == transaction.TransactionDate.Year);
                var maxWithdrawPerDay = maxWithdraw.Sum(a => a.Amount);
                var maxFrequency = maxWithdraw.Count();
                if (maxWithdrawPerDay > 150000)
                {
                    return BadRequest("Max Withdrawal Per Day Limit Reached");
                }
                if (transaction.Amount > 40000)
                {
                    return BadRequest("Max Withdrawal Per Transaction Reached");
                }
                if (maxFrequency > 4)
                {
                    return BadRequest("Max Withdrawal Frequency Per Day Reached");
                }
                
            }

            db.Transactions.Add(transaction);
            

            try
            {
                if (transaction.TransactionType.ToLower() == "deposit")
                {
                    account.Balance = account.Balance + transaction.Amount;
                }
                else
                {
                    account.Balance = account.Balance - transaction.Amount;
                }
                await db.SaveChangesAsync();
                
            }
            catch (DbUpdateException)
            {
                if (TransactionExists(transaction.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            db.Entry(transaction).Reference(x => x.BankAccount).Load();

            var dto = new TransactionDTO()
            {
                Id = transaction.Id,
                TransactionType = transaction.TransactionType,
                Amount = transaction.Amount,
                TransactionDate = transaction.TransactionDate,
                AccountOwner = transaction.BankAccountName,
                Balance = account.Balance
            };


            return CreatedAtRoute("DefaultApi", new { id = transaction.Id }, dto);
        }

        // DELETE: api/Transactions/5
        [ResponseType(typeof(Transaction))]
        public async Task<IHttpActionResult> DeleteTransaction(long id)
        {
            Transaction transaction = await db.Transactions.FindAsync(id);
            if (transaction == null)
            {
                return NotFound();
            }

            db.Transactions.Remove(transaction);
            await db.SaveChangesAsync();

            return Ok(transaction);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TransactionExists(long id)
        {
            return db.Transactions.Count(e => e.Id == id) > 0;
        }
    }
}