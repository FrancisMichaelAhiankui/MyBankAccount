namespace BankAccount.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initials : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Transactions", "TransactionDate", c => c.DateTimeOffset(nullable: false, precision: 7));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Transactions", "TransactionDate", c => c.DateTime(nullable: false));
        }
    }
}
