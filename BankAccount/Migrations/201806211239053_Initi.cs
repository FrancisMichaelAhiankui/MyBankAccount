namespace BankAccount.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initi : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Transactions", "BankAccountId", "dbo.Accounts");
            DropIndex("dbo.Transactions", new[] { "BankAccountId" });
            RenameColumn(table: "dbo.Transactions", name: "BankAccountId", newName: "BankAccount_Id");
            AddColumn("dbo.Transactions", "BankAccountName", c => c.String());
            AlterColumn("dbo.Transactions", "BankAccount_Id", c => c.Long());
            CreateIndex("dbo.Transactions", "BankAccount_Id");
            AddForeignKey("dbo.Transactions", "BankAccount_Id", "dbo.Accounts", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Transactions", "BankAccount_Id", "dbo.Accounts");
            DropIndex("dbo.Transactions", new[] { "BankAccount_Id" });
            AlterColumn("dbo.Transactions", "BankAccount_Id", c => c.Long(nullable: false));
            DropColumn("dbo.Transactions", "BankAccountName");
            RenameColumn(table: "dbo.Transactions", name: "BankAccount_Id", newName: "BankAccountId");
            CreateIndex("dbo.Transactions", "BankAccountId");
            AddForeignKey("dbo.Transactions", "BankAccountId", "dbo.Accounts", "Id", cascadeDelete: true);
        }
    }
}
