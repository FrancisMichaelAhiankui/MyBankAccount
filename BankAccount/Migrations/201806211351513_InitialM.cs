namespace BankAccount.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialM : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transactions", "BackAccountId", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transactions", "BackAccountId");
        }
    }
}
