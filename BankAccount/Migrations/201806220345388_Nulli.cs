namespace BankAccount.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Nulli : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transactions", "BankAccountBalance", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transactions", "BankAccountBalance");
        }
    }
}
