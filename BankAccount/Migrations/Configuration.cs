namespace BankAccount.Migrations
{
    using BankAccount.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BankAccount.Models.AccountContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(BankAccount.Models.AccountContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            
            DateTime dateTime = DateTime.Now;
            context.Accounts.AddOrUpdate(
                a => a.Id,
                new Account() {Id = 453245 , Name = "Mr Kwame",Balance=5000 },
                new Account() {Id = 245642 , Name = "Dr. Linus" ,Balance = 3000},
                new Account() { Id = 255642, Name = "Mike", Balance = 3000 });

            context.Transactions.AddOrUpdate(
                a => a.Id,
                new Transaction() {Id = 6582548,BackAccountId = 255642,  BankAccountName = "Mike", TransactionDate = dateTime , Amount = 48.7f ,TransactionType="withdrawal"  },
                new Transaction() { Id = 6582547, BackAccountId = 255642, BankAccountName = "Mike", TransactionDate = dateTime, Amount = 465.7f, TransactionType = "withdrawal" },
                new Transaction() { Id = 6585648, BackAccountId = 255642, BankAccountName = "Mike", TransactionDate = dateTime, Amount = 427.7f, TransactionType = "deposit" },
                new Transaction() { Id = 6342548, BackAccountId = 255642, BankAccountName = "Mike", TransactionDate = dateTime, Amount = 48.7f, TransactionType = "withdrawal" },
                new Transaction() { Id = 65647548, BackAccountId = 255642, BankAccountName = "Mike", TransactionDate = dateTime, Amount = 48.7f, TransactionType = "withdrawal" });
        }
    }
}
