﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BankAccount.Models
{
    //model of the account
    public class Account
    {
        [Required]
        public long Id { get; set; }
        public String Name { get; set; }
        public String AccountName { get;set; }
        public Double Balance { get; set; }

    }

    //used to get the account balance
    public class AccountBalance
    {
        public String Name { get; set; }
        public Double Balance { get; set; }

    }
}