﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BankAccount.Models
{
    //transaction model with transactiontype of withdraw or deposit
    public class Transaction
    {
        [Required]
        public long Id { get; set; }
        public String TransactionType { get; set; }
        public DateTimeOffset TransactionDate { get; set; }
        public double Amount { get; set; }
        public string BankAccountName { get; set; }
        public long BackAccountId { get; set; }
        public Double BankAccountBalance { get; set; }
        public Account BankAccount { get; set; }
    }

    public class TransactionDTO
    {
        [Required]
        public long Id { get; set; }
        public String TransactionType { get; set; }
        public DateTimeOffset TransactionDate { get; set; }
        public Double Amount { get; set; }
        public String AccountOwner { get; set; }
        public Double Balance { get; set; }

    }
    public class GetTransactionDTO
    {
        [Required]
        public long Id { get; set; }
        public String TransactionType { get; set; }
        public DateTimeOffset TransactionDate { get; set; }
        public Double Amount { get; set; }
        public String AccountOwner { get; set; }
        

    }

}